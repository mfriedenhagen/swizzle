/**
 *
 * Copyright 2006 David Blevins
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.codehaus.swizzle.jira;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;

/**
 * @version $Revision$ $Date$
 */
public class JiraIT extends SwizzleJiraTestCase {

    public static final String ISSUE_KEY = "MSHARED-490";
    // Date is retrieved without timezone from xmlrpc (@codehaus?)
    SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.ENGLISH);

    public void testJiraStatus() throws Exception {
        JiraReadOnly jira = getJira();
        assertEquals("https://issues.apache.org/jira", jira.getServerInfo().getBaseUrl());
    }

    public void testJira() throws Exception {
        JiraReadOnly jira = getJira();

        Issue issue = jira.getIssue(ISSUE_KEY);
        assertEquals("Issue.getKey()", ISSUE_KEY, issue.getKey());
        assertEquals("Issue.getCreated()", "Tue Jan 05 08:54:38 2016", formatter.format(issue.getCreated()));
    }

    public void testFill() throws Exception {
        Jira jira = getJira();

        Issue issue = jira.getIssue(ISSUE_KEY);
        issue = jira.fill(issue);

        assertEquals("Issue.getCreated()", "Fri Aug 04 20:05:13 2006", formatter.format(issue.getCreated()));
        assertEquals("Issue.getSummary()", "Unit Test Summary", issue.getSummary());
        assertEquals("Issue.getType()", "New Feature", issue.getType().getName());
        assertEquals("Issue.getEnvironment()", "Unit Test Environment", issue.getEnvironment());
        assertEquals("Issue.getStatus()", "Closed", issue.getStatus().getName());
        assertEquals("Issue.getUpdated()", "Fri Aug 04 21:33:48 2006", formatter.format(issue.getUpdated()));
        assertEquals("Issue.getId()", 40099, issue.getId());
        assertEquals("Issue.getKey()", "SWIZZLE-1", issue.getKey());
        assertEquals("Issue.getDescription()", "Unit Test Description", issue.getDescription());
        assertEquals("Issue.getDuedate()", "Sun Aug 06 00:00:00 2006", formatter.format(issue.getDuedate()));
        assertEquals("Issue.getReporter()", "David Blevins", issue.getReporter().getFullname());
        assertEquals("Issue.getProject()", "SWIZZLE", issue.getProject().getKey());
        assertEquals("Issue.getResolution()", "Fixed", issue.getResolution().getName());
        assertEquals("Issue.getVotes()", 1, issue.getVotes());
        assertEquals("Issue.getAssignee()", "David Blevins", issue.getAssignee().getFullname());
        assertEquals("Issue.getPriority()", "Blocker", issue.getPriority().getName());
        assertEquals("Issue.getLink()", "https://jira.codehaus.org/browse/SWIZZLE-1", issue.getLink());

        Map data = issue.toMap();

        assertEquals("issue.Created", "2006-08-04 20:05:13.157", data.get("created"));
        assertEquals("issue.Summary", "Unit Test Summary", data.get("summary"));
        assertEquals("issue.Type", "2", data.get("type"));
        assertEquals("issue.Environment", "Unit Test Environment", data.get("environment"));
        assertEquals("issue.Status", "6", data.get("status"));
        assertEquals("issue.Updated", "2006-08-04 21:33:48.108", data.get("updated"));
        assertEquals("issue.Id", "40099", data.get("id"));
        assertEquals("issue.Key", "SWIZZLE-1", data.get("key"));
        assertEquals("issue.Description", "Unit Test Description", data.get("description"));
        assertEquals("issue.Duedate", "2006-08-06 00:00:00.0", data.get("duedate"));
        assertEquals("issue.Reporter", "dblevins", data.get("reporter"));
        assertEquals("issue.Project", "SWIZZLE", data.get("project"));
        assertEquals("issue.Resolution", "1", data.get("resolution"));
        assertEquals("issue.Votes", "1", data.get("votes"));
        assertEquals("issue.Assignee", "dblevins", data.get("assignee"));
        assertEquals("issue.Priority", "1", data.get("priority"));
        assertEquals("issue.Link", null, data.get("link"));

    }

    public void testAutofillProject() throws Exception {
        Jira jira = getJira();
        jira.autofill("project", true);

        Issue issue = jira.getIssue(ISSUE_KEY);
        Project project = issue.getProject();

        assertEquals("Project.getId()", 12317922, project.getId());
        assertEquals("Project.getKey()", "MSHARED", project.getKey());
        assertEquals("Project.getName()", "Maven Shared Components", project.getName());
        assertEquals("Project.getLead()", null, project.getLead());
        assertEquals("Project.getProjectUrl() - " + project.getProjectUrl(), "http://swizzle.codehaus.org", project.getProjectUrl());
        assertEquals("Project.getUrl() - " + project.getUrl(), "https://jira.codehaus.org/browse/SWIZZLE", project.getUrl());
    }

}
